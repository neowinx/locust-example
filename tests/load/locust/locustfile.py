from locust import task, run_single_user
from locust import FastHttpUser


class SuperNeowinxTech(FastHttpUser):
    host = "https://super.neowinx.tech"
    token: str = ''
    default_headers = {
        "Accept": "application/json, text/plain, */*",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.9",
        "Connection": "keep-alive",
        "Content-Type": "application/json",
        "Host": "super.neowinx.tech",
        "Origin": "https://super.neowinx.tech",
        "Referer": "https://super.neowinx.tech/",
        "Sec-Fetch-Dest": "empty",
        "Sec-Fetch-Mode": "cors",
        "Sec-Fetch-Site": "same-origin",
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
        "sec-ch-ua": '"Not)A;Brand";v="24", "Chromium";v="116"',
        "sec-ch-ua-mobile": "?0",
        "sec-ch-ua-platform": '"Linux"',
    }

    @task
    def t(self):
        with self.rest(
            "POST",
            "/api/login",
            headers={
                "Accept": "application/json",
                "Content-Type": "application/json",
            },
            json={"username": "test", "password": "test"},
        ) as resp:
            self.token = resp.js['access_token']
        with self.rest(
            "POST",
            "/api/search/association?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass
        with self.rest(
            "PUT",
            "/api/association/1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={"id": 1, "name": "A.C.R.O.S.T.I.CA"},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/search/association?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/search/hero?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass
        with self.rest(
            "PUT",
            "/api/hero/5",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={"id": 5, "name": "Greenlanter"},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/search/hero?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/search/power?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/power",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={"name": "Telepathy"},
        ) as resp:
            pass
        with self.rest(
            "POST",
            "/api/search/power?page=1&rowsPerPage=10&sortBy=&descending=true&jsondepth=1",
            headers={
                "Authorization": f"Bearer {self.token}",
            },
            json={},
        ) as resp:
            pass


if __name__ == "__main__":
    run_single_user(super_neowinx_tech)
