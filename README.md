# Locust App de Ejemplo

Este es un ejemplo que contiene una app backend / frontend básica con la intención de poder ejecutar los tests con [Locust.io](https://locust.io/)
dentro del directorio `tests/load/locust`

## Requisitos

- [Git](https://git-scm.com/)
- [Docker](https://docs.docker.com/engine/install/) con [docker compose](https://docs.docker.com/compose/install/)

## Uso

Clonamos el repositorio con Git:

```bash
git clone https://gitlab.com/neowinx/locust-example.git
cd locust-example/tests/load/locust
```

Luego para iniciar locust y ejecutar los tests contra [super.neowinx.tech](http://super.neowinx.tech) usamos Docker

### Iniciar Locust...

#### ... con Docker Compose

```bash
docker-compose up
```

#### ... con Docker (sin docker-compose)

Para iniciar locust con docker directamente, podemos ejecutar el siguiente comando

```bash
docker run -ti --rm -v ./locustfile.py:/locustfile.py -p 8089:8089 locustio/locust:latest -f /locustfile.py
```

Una vez iniciado Locust, ingresamos a la url [localhost:8089](localhost:8089) en nuestro navegador web, y se nos presenta
la interfaz web de Locust

![locust-webpage](docs/img/locust-webpage.png)

Le damos click en el boton `Start Swarming` para iniciar el test de carga con los parametros que consideremos adecuados

## Enjoy

neowinx
