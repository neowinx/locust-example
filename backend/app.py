from resources.hero_universe import HeroUniverse, HeroUniverseList, HeroUniverseSearch
from resources.association import Association, AssociationList, AssociationSearch
from resources.association_universe import AssociationUniverse, AssociationUniverseList, AssociationUniverseSearch
from resources.user import User, UserList, UserSearch
from resources.power import Power, PowerList, PowerSearch
from resources.universe_era import UniverseEra, UniverseEraList, UniverseEraSearch
from resources.hero_association import HeroAssociation, HeroAssociationList, HeroAssociationSearch
from resources.universe import Universe, UniverseList, UniverseSearch
from resources.hero import Hero, HeroList, HeroSearch
from resources.hero_power import HeroPower, HeroPowerList, HeroPowerSearch
from resources.era import Era, EraList, EraSearch
import os
from db import db
from flasgger import Swagger, swag_from
from flask import Flask, jsonify, request, redirect
from flask_cors import CORS
from flask_jwt_extended import (
    JWTManager, jwt_required, create_access_token,
    get_jwt_identity,
    get_jwt)
from flask_restful import Api, Resource
from utils import JSONEncoder, unique_md5, JSONDecoder


permisions = [
    'hero_universe_list',
    'hero_universe_search',
    'hero_universe_get',
    'hero_universe_insert',
    'hero_universe_update',
    'hero_universe_delete',
    'association_list',
    'association_search',
    'association_get',
    'association_insert',
    'association_update',
    'association_delete',
    'association_universe_list',
    'association_universe_search',
    'association_universe_get',
    'association_universe_insert',
    'association_universe_update',
    'association_universe_delete',
    'user_list',
    'user_search',
    'user_get',
    'user_insert',
    'user_update',
    'user_delete',
    'universe_era_list',
    'universe_era_search',
    'universe_era_get',
    'universe_era_insert',
    'universe_era_update',
    'universe_era_delete',
    'hero_association_list',
    'hero_association_search',
    'hero_association_get',
    'hero_association_insert',
    'hero_association_update',
    'hero_association_delete',
    'universe_list',
    'universe_search',
    'universe_get',
    'universe_insert',
    'universe_update',
    'universe_delete',
    'hero_list',
    'hero_search',
    'hero_get',
    'hero_insert',
    'hero_update',
    'hero_delete',
    'power_list',
    'power_search',
    'power_get',
    'power_insert',
    'power_update',
    'power_delete',
    'hero_power_list',
    'hero_power_search',
    'hero_power_get',
    'hero_power_insert',
    'hero_power_update',
    'hero_power_delete',
    'era_list',
    'era_search',
    'era_get',
    'era_insert',
    'era_update',
    'era_delete',
]

PREFIX = '/api'

app = Flask(__name__)
CORS(app, supports_credentials=True)
api = Api(app, errors={
    'NoAuthorizationError': {
        "message": "Request does not contain an access token.",
        'error': 'authorization_required',
        'status': 401
    }
})

app.config['RESTFUL_JSON'] = {'cls': JSONEncoder}
app.json_encoder = JSONEncoder
app.json_decoder = JSONDecoder


@app.errorhandler(404)
def handle_404_error(e):
    return jsonify({
        "description": "You seem lost...",
        'error': 'resource_not_found'
    }), 404


@app.errorhandler(400)
def handle_400_error(e):
    return jsonify({
        "description": "I don't understand this, please send it right.. appreciated!",
        'error': 'bad_request'
    }), 404


# Function to facilitate the app configuration from environment variables
def env_config(name, default):
    app.config[name] = os.environ.get(name, default=default)


# Database config
env_config('SQLALCHEMY_DATABASE_URI', 'postgresql://postgres:postgres@localhost:5432/super')
# app.config['SQLALCHEMY_BINDS'] = {
#     'anotherdb':        'postgresql://postgres:postgres@localhost:5432/anotherdb'
# }
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True
app.config['SQLALCHEMY_ECHO'] = False


# Swagger config
app.config['SWAGGER'] = {
    'title': 'Super',
    'version': '2.0.0',
    'description': 'API de servicios REST en Flask',
    'uiversion': 2,
    'tags': [{'name': 'jwt'}]
}
swagger = Swagger(app)


# Setup the Flask-JWT-Extended extension
app.config['JWT_SECRET_KEY'] = 'super-secret'
app.config['JWT_ERROR_MESSAGE_KEY'] = 'error'
app.config['JWT_BLACKLIST_ENABLED'] = True
app.config['JWT_BLACKLIST_TOKEN_CHECKS'] = ['access', 'refresh']
jwt = JWTManager(app)

# tokens blacklist (replace this with database table or a redis cluster in production)
blacklist = set()
# Change this to get permissions from your permission store (database, redis, cache, file, etc.)
@jwt.additional_claims_loader
def add_claims_to_access_token(identity):
    return {
        'permisions': permisions
    }



@jwt.token_in_blocklist_loader
def check_if_token_in_blacklist(jwt_header, jwt_payload:dict):
    jti = jwt_payload['jti']
    # TODO: Replace this with a centralized cache service (Redis, Mongo, a database, etc.)
    return jti in blacklist


######################
# Application routes #
######################

@app.route(f'{PREFIX}')
@app.route(f'{PREFIX}/')
def welcome():
    return redirect("/apidocs", code=302)


# Provide a method to create access tokens. The create_access_token()
# function is used to actually generate the token, and you can return
# it to the caller however you choose.
@app.route(f'{PREFIX}/login', methods=['POST'])
@swag_from('swagger/flask_jwt_extended/login.yaml')
def login():
    if not request.is_json:
        return jsonify({"error": "Bad request"}), 400

    username = request.json.get('username', None)
    password = request.json.get('password', None)
    if not username or not password:
        return jsonify({"error": "Bad username or password. This incident will be registered"}), 400

    if username != 'test' or password != 'test':
        return jsonify({"error": "Bad username or password. This incident will be registered"}), 401

    # Identity can be any data that is json serializable
    access_token = create_access_token(identity=username)
    return jsonify(access_token=access_token), 200


@app.route(f'{PREFIX}/logout', methods=['DELETE'])
@jwt_required()
@swag_from('swagger/flask_jwt_extended/logout.yaml')
def logout():
    jti = get_jwt()['jti']
    blacklist.add(jti)
    return jsonify({"msg": "Successfully logged out"}), 200


# Protect a view with jwt_required, which requires a valid access token
# in the request to access.
@app.route(f'{PREFIX}/protected', methods=['GET'])
@jwt_required()
@swag_from('swagger/protected/example.yaml')
def protected():
    # Access the identity of the current user with get_jwt_identity
    current_user = get_jwt_identity()
    return jsonify(logged_in_as=current_user), 200


api.add_resource(Era, f'{PREFIX}/era/<id>')
api.add_resource(EraList, f'{PREFIX}/era')
api.add_resource(EraSearch, f'{PREFIX}/search/era')

api.add_resource(HeroPower, f'{PREFIX}/hero_power/<id>')
api.add_resource(HeroPowerList, f'{PREFIX}/hero_power')
api.add_resource(HeroPowerSearch, f'{PREFIX}/search/hero_power')

api.add_resource(Hero, f'{PREFIX}/hero/<id>')
api.add_resource(HeroList, f'{PREFIX}/hero')
api.add_resource(HeroSearch, f'{PREFIX}/search/hero')

api.add_resource(Universe, f'{PREFIX}/universe/<id>')
api.add_resource(UniverseList, f'{PREFIX}/universe')
api.add_resource(UniverseSearch, f'{PREFIX}/search/universe')

api.add_resource(HeroAssociation, f'{PREFIX}/hero_association/<id>')
api.add_resource(HeroAssociationList, f'{PREFIX}/hero_association')
api.add_resource(HeroAssociationSearch, f'{PREFIX}/search/hero_association')

api.add_resource(UniverseEra, f'{PREFIX}/universe_era/<id>')
api.add_resource(UniverseEraList, f'{PREFIX}/universe_era')
api.add_resource(UniverseEraSearch, f'{PREFIX}/search/universe_era')

api.add_resource(Power, f'{PREFIX}/power/<id>')
api.add_resource(PowerList, f'{PREFIX}/power')
api.add_resource(PowerSearch, f'{PREFIX}/search/power')

api.add_resource(User, f'{PREFIX}/user/<id>')
api.add_resource(UserList, f'{PREFIX}/user')
api.add_resource(UserSearch, f'{PREFIX}/search/user')

api.add_resource(AssociationUniverse, f'{PREFIX}/association_universe/<id>')
api.add_resource(AssociationUniverseList, f'{PREFIX}/association_universe')
api.add_resource(AssociationUniverseSearch, f'{PREFIX}/search/association_universe')

api.add_resource(Association, f'{PREFIX}/association/<id>')
api.add_resource(AssociationList, f'{PREFIX}/association')
api.add_resource(AssociationSearch, f'{PREFIX}/search/association')

api.add_resource(HeroUniverse, f'{PREFIX}/hero_universe/<id>')
api.add_resource(HeroUniverseList, f'{PREFIX}/hero_universe')
api.add_resource(HeroUniverseSearch, f'{PREFIX}/search/hero_universe')

if __name__ == '__main__':
    db.init_app(app)
    app.run(host=os.environ.get("FLASK_HOST", default="localhost"), port=os.environ.get("FLASK_PORT", default=5000))
# this lines are required for debugging with pycharm (although you can delete them if you want)
else:
    db.init_app(app)
