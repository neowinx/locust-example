import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.user import UserModel
from utils import restrict, check, paginated_results


class User(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('user', type=str)

    @jwt_required()
    @check('user_get')
    @swag_from('../swagger/user/get_user.yaml')
    def get(self, id):
        user = UserModel.find_by_id(id)
        if user:
            return user.json()
        return {'message': 'No se encuentra User'}, 404

    @jwt_required()
    @check('user_update')
    @swag_from('../swagger/user/put_user.yaml')
    def put(self, id):
        user = UserModel.find_by_id(id)
        if user:
            newdata = User.parser.parse_args()
            UserModel.from_reqparse(user, newdata)
            user.save_to_db()
            return user.json()
        return {'message': 'No se encuentra User'}, 404

    @jwt_required()
    @check('user_delete')
    @swag_from('../swagger/user/delete_user.yaml')
    def delete(self, id):
        user = UserModel.find_by_id(id)
        if user:
            user.delete_from_db()

        return {'message': 'Se ha borrado User'}


class UserList(Resource):

    @jwt_required()
    @check('user_list')
    @swag_from('../swagger/user/list_user.yaml')
    def get(self):
        query = UserModel.query
        return paginated_results(query)

    @jwt_required()
    @check('user_insert')
    @swag_from('../swagger/user/post_user.yaml')
    def post(self):
        data = User.parser.parse_args()

        id = data.get('id')

        if id is not None and UserModel.find_by_id(id):
            return {'message': "Ya existe un user con id '{}'.".format(id)}, 400

        user = UserModel(**data)
        try:
            user.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear User."}, 500

        return user.json(), 201


class UserSearch(Resource):

    @jwt_required()
    @check('user_search')
    @swag_from('../swagger/user/search_user.yaml')
    def post(self):
        query = UserModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: UserModel.id == x)
            query = restrict(query, filters, 'user', lambda x: UserModel.user.contains(x))
        return paginated_results(query)
