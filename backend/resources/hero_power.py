import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.hero_power import HeroPowerModel
from utils import restrict, check, paginated_results


class HeroPower(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('hero_id', type=int)
    parser.add_argument('power_id', type=int)

    @jwt_required()
    @check('hero_power_get')
    @swag_from('../swagger/hero_power/get_hero_power.yaml')
    def get(self, id):
        hero_power = HeroPowerModel.find_by_id(id)
        if hero_power:
            return hero_power.json()
        return {'message': 'No se encuentra Hero_power'}, 404

    @jwt_required()
    @check('hero_power_update')
    @swag_from('../swagger/hero_power/put_hero_power.yaml')
    def put(self, id):
        hero_power = HeroPowerModel.find_by_id(id)
        if hero_power:
            newdata = HeroPower.parser.parse_args()
            HeroPowerModel.from_reqparse(hero_power, newdata)
            hero_power.save_to_db()
            return hero_power.json()
        return {'message': 'No se encuentra Hero_power'}, 404

    @jwt_required()
    @check('hero_power_delete')
    @swag_from('../swagger/hero_power/delete_hero_power.yaml')
    def delete(self, id):
        hero_power = HeroPowerModel.find_by_id(id)
        if hero_power:
            hero_power.delete_from_db()

        return {'message': 'Se ha borrado Hero_power'}


class HeroPowerList(Resource):

    @jwt_required()
    @check('hero_power_list')
    @swag_from('../swagger/hero_power/list_hero_power.yaml')
    def get(self):
        query = HeroPowerModel.query
        return paginated_results(query)

    @jwt_required()
    @check('hero_power_insert')
    @swag_from('../swagger/hero_power/post_hero_power.yaml')
    def post(self):
        data = HeroPower.parser.parse_args()

        id = data.get('id')

        if id is not None and HeroPowerModel.find_by_id(id):
            return {'message': "Ya existe un hero_power con id '{}'.".format(id)}, 400

        hero_power = HeroPowerModel(**data)
        try:
            hero_power.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Hero_power."}, 500

        return hero_power.json(), 201


class HeroPowerSearch(Resource):

    @jwt_required()
    @check('hero_power_search')
    @swag_from('../swagger/hero_power/search_hero_power.yaml')
    def post(self):
        query = HeroPowerModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: HeroPowerModel.id == x)
            query = restrict(query, filters, 'hero_id', lambda x: HeroPowerModel.hero_id == x)
            query = restrict(query, filters, 'power_id', lambda x: HeroPowerModel.power_id == x)
        return paginated_results(query)
