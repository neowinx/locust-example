import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.era import EraModel
from utils import restrict, check, paginated_results


class Era(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('name', type=str)

    @jwt_required()
    @check('era_get')
    @swag_from('../swagger/era/get_era.yaml')
    def get(self, id):
        era = EraModel.find_by_id(id)
        if era:
            return era.json()
        return {'message': 'No se encuentra Era'}, 404

    @jwt_required()
    @check('era_update')
    @swag_from('../swagger/era/put_era.yaml')
    def put(self, id):
        era = EraModel.find_by_id(id)
        if era:
            newdata = Era.parser.parse_args()
            EraModel.from_reqparse(era, newdata)
            era.save_to_db()
            return era.json()
        return {'message': 'No se encuentra Era'}, 404

    @jwt_required()
    @check('era_delete')
    @swag_from('../swagger/era/delete_era.yaml')
    def delete(self, id):
        era = EraModel.find_by_id(id)
        if era:
            era.delete_from_db()

        return {'message': 'Se ha borrado Era'}


class EraList(Resource):

    @jwt_required()
    @check('era_list')
    @swag_from('../swagger/era/list_era.yaml')
    def get(self):
        query = EraModel.query
        return paginated_results(query)

    @jwt_required()
    @check('era_insert')
    @swag_from('../swagger/era/post_era.yaml')
    def post(self):
        data = Era.parser.parse_args()

        id = data.get('id')

        if id is not None and EraModel.find_by_id(id):
            return {'message': "Ya existe un era con id '{}'.".format(id)}, 400

        era = EraModel(**data)
        try:
            era.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Era."}, 500

        return era.json(), 201


class EraSearch(Resource):

    @jwt_required()
    @check('era_search')
    @swag_from('../swagger/era/search_era.yaml')
    def post(self):
        query = EraModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: EraModel.id == x)
            query = restrict(query, filters, 'name', lambda x: EraModel.name.contains(x))
        return paginated_results(query)
