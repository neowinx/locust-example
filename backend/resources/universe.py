import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.universe import UniverseModel
from utils import restrict, check, paginated_results


class Universe(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('name', type=str)

    @jwt_required()
    @check('universe_get')
    @swag_from('../swagger/universe/get_universe.yaml')
    def get(self, id):
        universe = UniverseModel.find_by_id(id)
        if universe:
            return universe.json()
        return {'message': 'No se encuentra Universe'}, 404

    @jwt_required()
    @check('universe_update')
    @swag_from('../swagger/universe/put_universe.yaml')
    def put(self, id):
        universe = UniverseModel.find_by_id(id)
        if universe:
            newdata = Universe.parser.parse_args()
            UniverseModel.from_reqparse(universe, newdata)
            universe.save_to_db()
            return universe.json()
        return {'message': 'No se encuentra Universe'}, 404

    @jwt_required()
    @check('universe_delete')
    @swag_from('../swagger/universe/delete_universe.yaml')
    def delete(self, id):
        universe = UniverseModel.find_by_id(id)
        if universe:
            universe.delete_from_db()

        return {'message': 'Se ha borrado Universe'}


class UniverseList(Resource):

    @jwt_required()
    @check('universe_list')
    @swag_from('../swagger/universe/list_universe.yaml')
    def get(self):
        query = UniverseModel.query
        return paginated_results(query)

    @jwt_required()
    @check('universe_insert')
    @swag_from('../swagger/universe/post_universe.yaml')
    def post(self):
        data = Universe.parser.parse_args()

        id = data.get('id')

        if id is not None and UniverseModel.find_by_id(id):
            return {'message': "Ya existe un universe con id '{}'.".format(id)}, 400

        universe = UniverseModel(**data)
        try:
            universe.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Universe."}, 500

        return universe.json(), 201


class UniverseSearch(Resource):

    @jwt_required()
    @check('universe_search')
    @swag_from('../swagger/universe/search_universe.yaml')
    def post(self):
        query = UniverseModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: UniverseModel.id == x)
            query = restrict(query, filters, 'name', lambda x: UniverseModel.name.contains(x))
        return paginated_results(query)
