import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.association_universe import AssociationUniverseModel
from utils import restrict, check, paginated_results


class AssociationUniverse(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('association_id', type=int)
    parser.add_argument('universe_id', type=int)

    @jwt_required()
    @check('association_universe_get')
    @swag_from('../swagger/association_universe/get_association_universe.yaml')
    def get(self, id):
        association_universe = AssociationUniverseModel.find_by_id(id)
        if association_universe:
            return association_universe.json()
        return {'message': 'No se encuentra Association_universe'}, 404

    @jwt_required()
    @check('association_universe_update')
    @swag_from('../swagger/association_universe/put_association_universe.yaml')
    def put(self, id):
        association_universe = AssociationUniverseModel.find_by_id(id)
        if association_universe:
            newdata = AssociationUniverse.parser.parse_args()
            AssociationUniverseModel.from_reqparse(association_universe, newdata)
            association_universe.save_to_db()
            return association_universe.json()
        return {'message': 'No se encuentra Association_universe'}, 404

    @jwt_required()
    @check('association_universe_delete')
    @swag_from('../swagger/association_universe/delete_association_universe.yaml')
    def delete(self, id):
        association_universe = AssociationUniverseModel.find_by_id(id)
        if association_universe:
            association_universe.delete_from_db()

        return {'message': 'Se ha borrado Association_universe'}


class AssociationUniverseList(Resource):

    @jwt_required()
    @check('association_universe_list')
    @swag_from('../swagger/association_universe/list_association_universe.yaml')
    def get(self):
        query = AssociationUniverseModel.query
        return paginated_results(query)

    @jwt_required()
    @check('association_universe_insert')
    @swag_from('../swagger/association_universe/post_association_universe.yaml')
    def post(self):
        data = AssociationUniverse.parser.parse_args()

        id = data.get('id')

        if id is not None and AssociationUniverseModel.find_by_id(id):
            return {'message': "Ya existe un association_universe con id '{}'.".format(id)}, 400

        association_universe = AssociationUniverseModel(**data)
        try:
            association_universe.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Association_universe."}, 500

        return association_universe.json(), 201


class AssociationUniverseSearch(Resource):

    @jwt_required()
    @check('association_universe_search')
    @swag_from('../swagger/association_universe/search_association_universe.yaml')
    def post(self):
        query = AssociationUniverseModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: AssociationUniverseModel.id == x)
            query = restrict(query, filters, 'association_id', lambda x: AssociationUniverseModel.association_id == x)
            query = restrict(query, filters, 'universe_id', lambda x: AssociationUniverseModel.universe_id == x)
        return paginated_results(query)
