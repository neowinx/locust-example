import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.association import AssociationModel
from utils import restrict, check, paginated_results


class Association(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('name', type=str)

    @jwt_required()
    @check('association_get')
    @swag_from('../swagger/association/get_association.yaml')
    def get(self, id):
        association = AssociationModel.find_by_id(id)
        if association:
            return association.json()
        return {'message': 'No se encuentra Association'}, 404

    @jwt_required()
    @check('association_update')
    @swag_from('../swagger/association/put_association.yaml')
    def put(self, id):
        association = AssociationModel.find_by_id(id)
        if association:
            newdata = Association.parser.parse_args()
            AssociationModel.from_reqparse(association, newdata)
            association.save_to_db()
            return association.json()
        return {'message': 'No se encuentra Association'}, 404

    @jwt_required()
    @check('association_delete')
    @swag_from('../swagger/association/delete_association.yaml')
    def delete(self, id):
        association = AssociationModel.find_by_id(id)
        if association:
            association.delete_from_db()

        return {'message': 'Se ha borrado Association'}


class AssociationList(Resource):

    @jwt_required()
    @check('association_list')
    @swag_from('../swagger/association/list_association.yaml')
    def get(self):
        query = AssociationModel.query
        return paginated_results(query)

    @jwt_required()
    @check('association_insert')
    @swag_from('../swagger/association/post_association.yaml')
    def post(self):
        data = Association.parser.parse_args()

        id = data.get('id')

        if id is not None and AssociationModel.find_by_id(id):
            return {'message': "Ya existe un association con id '{}'.".format(id)}, 400

        association = AssociationModel(**data)
        try:
            association.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Association."}, 500

        return association.json(), 201


class AssociationSearch(Resource):

    @jwt_required()
    @check('association_search')
    @swag_from('../swagger/association/search_association.yaml')
    def post(self):
        query = AssociationModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: AssociationModel.id == x)
            query = restrict(query, filters, 'name', lambda x: AssociationModel.name.contains(x))
        return paginated_results(query)
