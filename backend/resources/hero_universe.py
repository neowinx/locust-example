import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.hero_universe import HeroUniverseModel
from utils import restrict, check, paginated_results


class HeroUniverse(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('hero_id', type=int)
    parser.add_argument('universe_id', type=int)

    @jwt_required()
    @check('hero_universe_get')
    @swag_from('../swagger/hero_universe/get_hero_universe.yaml')
    def get(self, id):
        hero_universe = HeroUniverseModel.find_by_id(id)
        if hero_universe:
            return hero_universe.json()
        return {'message': 'No se encuentra Hero_universe'}, 404

    @jwt_required()
    @check('hero_universe_update')
    @swag_from('../swagger/hero_universe/put_hero_universe.yaml')
    def put(self, id):
        hero_universe = HeroUniverseModel.find_by_id(id)
        if hero_universe:
            newdata = HeroUniverse.parser.parse_args()
            HeroUniverseModel.from_reqparse(hero_universe, newdata)
            hero_universe.save_to_db()
            return hero_universe.json()
        return {'message': 'No se encuentra Hero_universe'}, 404

    @jwt_required()
    @check('hero_universe_delete')
    @swag_from('../swagger/hero_universe/delete_hero_universe.yaml')
    def delete(self, id):
        hero_universe = HeroUniverseModel.find_by_id(id)
        if hero_universe:
            hero_universe.delete_from_db()

        return {'message': 'Se ha borrado Hero_universe'}


class HeroUniverseList(Resource):

    @jwt_required()
    @check('hero_universe_list')
    @swag_from('../swagger/hero_universe/list_hero_universe.yaml')
    def get(self):
        query = HeroUniverseModel.query
        return paginated_results(query)

    @jwt_required()
    @check('hero_universe_insert')
    @swag_from('../swagger/hero_universe/post_hero_universe.yaml')
    def post(self):
        data = HeroUniverse.parser.parse_args()

        id = data.get('id')

        if id is not None and HeroUniverseModel.find_by_id(id):
            return {'message': "Ya existe un hero_universe con id '{}'.".format(id)}, 400

        hero_universe = HeroUniverseModel(**data)
        try:
            hero_universe.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Hero_universe."}, 500

        return hero_universe.json(), 201


class HeroUniverseSearch(Resource):

    @jwt_required()
    @check('hero_universe_search')
    @swag_from('../swagger/hero_universe/search_hero_universe.yaml')
    def post(self):
        query = HeroUniverseModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: HeroUniverseModel.id == x)
            query = restrict(query, filters, 'hero_id', lambda x: HeroUniverseModel.hero_id == x)
            query = restrict(query, filters, 'universe_id', lambda x: HeroUniverseModel.universe_id == x)
        return paginated_results(query)
