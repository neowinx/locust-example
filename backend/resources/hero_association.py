import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.hero_association import HeroAssociationModel
from utils import restrict, check, paginated_results


class HeroAssociation(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('hero_id', type=int)
    parser.add_argument('association_id', type=int)

    @jwt_required()
    @check('hero_association_get')
    @swag_from('../swagger/hero_association/get_hero_association.yaml')
    def get(self, id):
        hero_association = HeroAssociationModel.find_by_id(id)
        if hero_association:
            return hero_association.json()
        return {'message': 'No se encuentra Hero_association'}, 404

    @jwt_required()
    @check('hero_association_update')
    @swag_from('../swagger/hero_association/put_hero_association.yaml')
    def put(self, id):
        hero_association = HeroAssociationModel.find_by_id(id)
        if hero_association:
            newdata = HeroAssociation.parser.parse_args()
            HeroAssociationModel.from_reqparse(hero_association, newdata)
            hero_association.save_to_db()
            return hero_association.json()
        return {'message': 'No se encuentra Hero_association'}, 404

    @jwt_required()
    @check('hero_association_delete')
    @swag_from('../swagger/hero_association/delete_hero_association.yaml')
    def delete(self, id):
        hero_association = HeroAssociationModel.find_by_id(id)
        if hero_association:
            hero_association.delete_from_db()

        return {'message': 'Se ha borrado Hero_association'}


class HeroAssociationList(Resource):

    @jwt_required()
    @check('hero_association_list')
    @swag_from('../swagger/hero_association/list_hero_association.yaml')
    def get(self):
        query = HeroAssociationModel.query
        return paginated_results(query)

    @jwt_required()
    @check('hero_association_insert')
    @swag_from('../swagger/hero_association/post_hero_association.yaml')
    def post(self):
        data = HeroAssociation.parser.parse_args()

        id = data.get('id')

        if id is not None and HeroAssociationModel.find_by_id(id):
            return {'message': "Ya existe un hero_association con id '{}'.".format(id)}, 400

        hero_association = HeroAssociationModel(**data)
        try:
            hero_association.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Hero_association."}, 500

        return hero_association.json(), 201


class HeroAssociationSearch(Resource):

    @jwt_required()
    @check('hero_association_search')
    @swag_from('../swagger/hero_association/search_hero_association.yaml')
    def post(self):
        query = HeroAssociationModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: HeroAssociationModel.id == x)
            query = restrict(query, filters, 'hero_id', lambda x: HeroAssociationModel.hero_id == x)
            query = restrict(query, filters, 'association_id', lambda x: HeroAssociationModel.association_id == x)
        return paginated_results(query)
