import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.universe_era import UniverseEraModel
from utils import restrict, check, paginated_results


class UniverseEra(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('era_id', type=int)
    parser.add_argument('universe_id', type=int)

    @jwt_required()
    @check('universe_era_get')
    @swag_from('../swagger/universe_era/get_universe_era.yaml')
    def get(self, id):
        universe_era = UniverseEraModel.find_by_id(id)
        if universe_era:
            return universe_era.json()
        return {'message': 'No se encuentra Universe_era'}, 404

    @jwt_required()
    @check('universe_era_update')
    @swag_from('../swagger/universe_era/put_universe_era.yaml')
    def put(self, id):
        universe_era = UniverseEraModel.find_by_id(id)
        if universe_era:
            newdata = UniverseEra.parser.parse_args()
            UniverseEraModel.from_reqparse(universe_era, newdata)
            universe_era.save_to_db()
            return universe_era.json()
        return {'message': 'No se encuentra Universe_era'}, 404

    @jwt_required()
    @check('universe_era_delete')
    @swag_from('../swagger/universe_era/delete_universe_era.yaml')
    def delete(self, id):
        universe_era = UniverseEraModel.find_by_id(id)
        if universe_era:
            universe_era.delete_from_db()

        return {'message': 'Se ha borrado Universe_era'}


class UniverseEraList(Resource):

    @jwt_required()
    @check('universe_era_list')
    @swag_from('../swagger/universe_era/list_universe_era.yaml')
    def get(self):
        query = UniverseEraModel.query
        return paginated_results(query)

    @jwt_required()
    @check('universe_era_insert')
    @swag_from('../swagger/universe_era/post_universe_era.yaml')
    def post(self):
        data = UniverseEra.parser.parse_args()

        id = data.get('id')

        if id is not None and UniverseEraModel.find_by_id(id):
            return {'message': "Ya existe un universe_era con id '{}'.".format(id)}, 400

        universe_era = UniverseEraModel(**data)
        try:
            universe_era.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Universe_era."}, 500

        return universe_era.json(), 201


class UniverseEraSearch(Resource):

    @jwt_required()
    @check('universe_era_search')
    @swag_from('../swagger/universe_era/search_universe_era.yaml')
    def post(self):
        query = UniverseEraModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: UniverseEraModel.id == x)
            query = restrict(query, filters, 'era_id', lambda x: UniverseEraModel.era_id == x)
            query = restrict(query, filters, 'universe_id', lambda x: UniverseEraModel.universe_id == x)
        return paginated_results(query)
