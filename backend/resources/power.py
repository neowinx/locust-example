import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.power import PowerModel
from utils import restrict, check, paginated_results


class Power(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('name', type=str)

    @jwt_required()
    @check('power_get')
    @swag_from('../swagger/power/get_power.yaml')
    def get(self, id):
        power = PowerModel.find_by_id(id)
        if power:
            return power.json()
        return {'message': 'No se encuentra Power'}, 404

    @jwt_required()
    @check('power_update')
    @swag_from('../swagger/power/put_power.yaml')
    def put(self, id):
        power = PowerModel.find_by_id(id)
        if power:
            newdata = Power.parser.parse_args()
            PowerModel.from_reqparse(power, newdata)
            power.save_to_db()
            return power.json()
        return {'message': 'No se encuentra Power'}, 404

    @jwt_required()
    @check('power_delete')
    @swag_from('../swagger/power/delete_power.yaml')
    def delete(self, id):
        power = PowerModel.find_by_id(id)
        if power:
            power.delete_from_db()

        return {'message': 'Se ha borrado Power'}


class PowerList(Resource):

    @jwt_required()
    @check('power_list')
    @swag_from('../swagger/power/list_power.yaml')
    def get(self):
        query = PowerModel.query
        return paginated_results(query)

    @jwt_required()
    @check('power_insert')
    @swag_from('../swagger/power/post_power.yaml')
    def post(self):
        data = Power.parser.parse_args()

        id = data.get('id')

        if id is not None and PowerModel.find_by_id(id):
            return {'message': "Ya existe un power con id '{}'.".format(id)}, 400

        power = PowerModel(**data)
        try:
            power.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Power."}, 500

        return power.json(), 201


class PowerSearch(Resource):

    @jwt_required()
    @check('power_search')
    @swag_from('../swagger/power/search_power.yaml')
    def post(self):
        query = PowerModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: PowerModel.id == x)
            query = restrict(query, filters, 'name', lambda x: PowerModel.name.contains(x))
        return paginated_results(query)
