import base64
import logging
from datetime import datetime

from flasgger import swag_from
from flask import request
from flask_jwt_extended import jwt_required
from flask_restful import Resource, reqparse
from models.hero import HeroModel
from utils import restrict, check, paginated_results


class Hero(Resource):

    parser = reqparse.RequestParser()
    parser.add_argument('id', type=int)
    parser.add_argument('name', type=str)

    @jwt_required()
    @check('hero_get')
    @swag_from('../swagger/hero/get_hero.yaml')
    def get(self, id):
        hero = HeroModel.find_by_id(id)
        if hero:
            return hero.json()
        return {'message': 'No se encuentra Hero'}, 404

    @jwt_required()
    @check('hero_update')
    @swag_from('../swagger/hero/put_hero.yaml')
    def put(self, id):
        hero = HeroModel.find_by_id(id)
        if hero:
            newdata = Hero.parser.parse_args()
            HeroModel.from_reqparse(hero, newdata)
            hero.save_to_db()
            return hero.json()
        return {'message': 'No se encuentra Hero'}, 404

    @jwt_required()
    @check('hero_delete')
    @swag_from('../swagger/hero/delete_hero.yaml')
    def delete(self, id):
        hero = HeroModel.find_by_id(id)
        if hero:
            hero.delete_from_db()

        return {'message': 'Se ha borrado Hero'}


class HeroList(Resource):

    @jwt_required()
    @check('hero_list')
    @swag_from('../swagger/hero/list_hero.yaml')
    def get(self):
        query = HeroModel.query
        return paginated_results(query)

    @jwt_required()
    @check('hero_insert')
    @swag_from('../swagger/hero/post_hero.yaml')
    def post(self):
        data = Hero.parser.parse_args()

        id = data.get('id')

        if id is not None and HeroModel.find_by_id(id):
            return {'message': "Ya existe un hero con id '{}'.".format(id)}, 400

        hero = HeroModel(**data)
        try:
            hero.save_to_db()
        except Exception as e:
            logging.error('Ocurrió un error al crear Cliente.', exc_info=e)
            return {"message": "Ocurrió un error al crear Hero."}, 500

        return hero.json(), 201


class HeroSearch(Resource):

    @jwt_required()
    @check('hero_search')
    @swag_from('../swagger/hero/search_hero.yaml')
    def post(self):
        query = HeroModel.query
        if request.json:
            filters = request.json
            query = restrict(query, filters, 'id', lambda x: HeroModel.id == x)
            query = restrict(query, filters, 'name', lambda x: HeroModel.name.contains(x))
        return paginated_results(query)
