import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel


class HeroModel(BaseModel):
    __tablename__ = 'hero'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)


    def __init__(self, id, name):
        self.id = id
        self.name = name

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
            'name': self.name,
        }

        
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

