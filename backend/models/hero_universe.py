import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel


class HeroUniverseModel(BaseModel):
    __tablename__ = 'hero_universe'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    hero_id = db.Column(db.Integer, nullable=False)
    universe_id = db.Column(db.Integer, nullable=False)


    def __init__(self, id, hero_id, universe_id):
        self.id = id
        self.hero_id = hero_id
        self.universe_id = universe_id

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
            'hero_id': self.hero_id,
            'universe_id': self.universe_id,
        }

        
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

