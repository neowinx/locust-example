import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel
from models.hero import HeroModel
from models.association import AssociationModel


class HeroAssociationModel(BaseModel):
    __tablename__ = 'hero_association'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    hero_id = db.Column(db.Integer, db.ForeignKey(HeroModel.id))
    association_id = db.Column(db.Integer, db.ForeignKey(AssociationModel.id))

    hero = db.relationship('HeroModel', foreign_keys=[hero_id], uselist=False)
    association = db.relationship('AssociationModel', foreign_keys=[association_id], uselist=False)

    def __init__(self, id, hero_id, association_id):
        self.id = id
        self.hero_id = hero_id
        self.association_id = association_id

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
        }

        if jsondepth > 0:
            if self.hero:
                json['hero'] = self.hero.json(jsondepth - 1)
            if self.association:
                json['association'] = self.association.json(jsondepth - 1)
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

