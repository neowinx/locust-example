import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel


class UserModel(BaseModel):
    __tablename__ = 'user'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    user = db.Column(db.String, nullable=False)


    def __init__(self, id, user):
        self.id = id
        self.user = user

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
            'user': self.user,
        }

        
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

