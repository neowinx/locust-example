import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel


class AssociationUniverseModel(BaseModel):
    __tablename__ = 'association_universe'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    association_id = db.Column(db.Integer, nullable=False)
    universe_id = db.Column(db.Integer, nullable=False)


    def __init__(self, id, association_id, universe_id):
        self.id = id
        self.association_id = association_id
        self.universe_id = universe_id

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
            'association_id': self.association_id,
            'universe_id': self.universe_id,
        }

        
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

