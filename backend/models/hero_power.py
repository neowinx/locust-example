import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel
from models.hero import HeroModel
from models.power import PowerModel


class HeroPowerModel(BaseModel):
    __tablename__ = 'hero_power'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    hero_id = db.Column(db.Integer, db.ForeignKey(HeroModel.id))
    power_id = db.Column(db.Integer, db.ForeignKey(PowerModel.id))

    hero = db.relationship('HeroModel', foreign_keys=[hero_id], uselist=False)
    power = db.relationship('PowerModel', foreign_keys=[power_id], uselist=False)

    def __init__(self, id, hero_id, power_id):
        self.id = id
        self.hero_id = hero_id
        self.power_id = power_id

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
        }

        if jsondepth > 0:
            if self.hero:
                json['hero'] = self.hero.json(jsondepth - 1)
            if self.power:
                json['power'] = self.power.json(jsondepth - 1)
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

