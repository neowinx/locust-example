import base64

from flask_restful.reqparse import Namespace

from db import db, BaseModel
from models.era import EraModel
from models.universe import UniverseModel


class UniverseEraModel(BaseModel):
    __tablename__ = 'universe_era'

    id = db.Column(db.Integer, primary_key=True, nullable=False)
    era_id = db.Column(db.Integer, db.ForeignKey(EraModel.id), nullable=False)
    universe_id = db.Column(db.Integer, db.ForeignKey(UniverseModel.id), nullable=False)

    era = db.relationship('EraModel', foreign_keys=[era_id], uselist=False)
    universe = db.relationship('UniverseModel', foreign_keys=[universe_id], uselist=False)

    def __init__(self, id, era_id, universe_id):
        self.id = id
        self.era_id = era_id
        self.universe_id = universe_id

    def json(self, jsondepth=0):
        json = {
            'id': self.id,
        }

        if jsondepth > 0:
            if self.era:
                json['era'] = self.era.json(jsondepth - 1)
            if self.universe:
                json['universe'] = self.universe.json(jsondepth - 1)
        return json

    @classmethod
    def find_by_id(cls, id):
        return cls.query.filter_by(id=id).first()

