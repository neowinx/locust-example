# Locust Example App

This is an example that contains a basic backend / frontend app intended to be tested with the locust.io tests
inside `tests/load/locust` directory

## Requirements

- [Git](https://git-scm.com/)
- [Docker](https://docs.docker.com/engine/install/) con [docker compose](https://docs.docker.com/compose/install/)

## Usage

Clone the Git repository

```bash
git clone https://gitlab.com/neowinx/locust-example.git
cd locust-example/tests/load/locust
```

After that, to start locust and execute the tests against [super.neowinx.tech](http://super.neowinx.tech) we use Docker

### Start Locust...

#### ... with Docker Compose

```bash
docker-compose up
```

#### ... with Docker (without docker-compose)

To start locust with directly with docker, we can run the next command

```bash
docker run -ti --rm -v ./locustfile.py:/locustfile.py -p 8089:8089 locustio/locust:latest -f /locustfile.py
```

Once Locust is strted, go to the url [localhost:8089](localhost:8089) in your web browser, and the locust web interface
appears

![locust-webpage](docs/img/locust-webpage.png)

Click on the `Start Swarming` button to start the load test with the parameters you considered addecuate

## Enjoy

neowinx
