#!/bin/bash

if [ "$STATIC_BACKEND_URL" != "" ]; then
  sed -i 's|STATIC_BACKEND_URL:"[a-zA-Z0-9\/:-_.&?]\+",|STATIC_BACKEND_URL:"'"$STATIC_BACKEND_URL"'",|g' /usr/share/nginx/html/assets/auth.*.js
fi

if [ "$BACKEND_URL" != "" ]; then
  sed -i 's|BACKEND_URL:"[a-zA-Z0-9\/:-_.&?]\+",|BACKEND_URL:"'"$BACKEND_URL"'",|g' /usr/share/nginx/html/assets/auth.*.js
fi

