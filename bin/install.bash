BASEDIR="$(dirname $0)/.."

echo "Checking prerequisites..."

if ! which docker; then
  echo "docker not found. Please install docker to proceed"
  exit 1
fi

DOCKER_COMPOSE="docker-compose"
if ! which "${DOCKER_COMPOSE}"; then
  DOCKER_COMPOSE="docker compose"
  if ! "$DOCKER_COMPOSE"  version; then
    echo "docker compose not installed. Please install docker-compose to proceed"
    exit 2
  fi
fi

cd $BASEDIR

for IMAGE in $(ls images/*.tar); do
docker load < "${IMAGE}"
done

echo "Starting up..."
DOCKER_COMPOSE up -d
