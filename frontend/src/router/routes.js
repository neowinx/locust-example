const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/IndexPage.vue') },
      { path: '/association', component: () => import('pages/AssociationPage.vue') },
      { path: '/universe', component: () => import('pages/UniversePage.vue') },
      { path: '/power', component: () => import('pages/PowerPage.vue') },
      { path: '/era', component: () => import('pages/EraPage.vue') },
      { path: '/hero', component: () => import('pages/HeroPage.vue') }
    ]
  },
  {
    path: '/login',
    component: () => import('layouts/BlankLayout.vue'),
    children: [
      { path: '', component: () => import('pages/LoginPage.vue') }
    ]
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/ErrorNotFound.vue')
  }
]

export default routes
